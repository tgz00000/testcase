import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/entity/movie_detail.dart';
import 'package:majootestcase/models/repository/i_movierepository.dart';
import 'package:majootestcase/services/datamodel/movie_response.dart';
import 'package:majootestcase/services/common/dio_config_service.dart'
    as dioConfig;

class ApiServices implements IMovieRepository {
  @override
  Future<List<MovieDetail>?> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response<String> response = await dio.get("");
      if (response.data != null) {
        MovieResponse movieResponse =
            MovieResponse.fromJson(jsonDecode(response.data!));

        return movieResponse.data;
      }
      return null;
    } catch (e) {
      //print(e.toString());
      return null;
    }
  }
}
