import 'dart:convert';

import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/services/common/database/stringdbconst.dart';

class UserData extends User {
  UserData(
      {required String email,
      required String username,
      required String password})
      : super(email: email, userName: username, password: password);

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'userName': userName,
      'password': password,
    };
  }

  factory UserData.fromUser(User user) {
    return UserData(
      email: user.email,
      username: user.userName,
      password: user.password,
    );
  }

  factory UserData.fromMapDb(Map<String, dynamic> map) {
    return UserData(
      email: map[TbUser.email] ?? '',
      username: map[TbUser.username] ?? '',
      password: map[TbUser.password] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  Map<String, dynamic> toMapDb() => {
        TbUser.email: email,
        TbUser.password: password,
        TbUser.username: userName
      };
}
