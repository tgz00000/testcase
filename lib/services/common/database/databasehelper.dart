import 'dart:async';

import 'package:majootestcase/services/common/database/stringdbconst.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database? _db;

  Future<Database?> get db async {
    if (_db != null) return _db;

    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    //   Sqflite.devSetDebugModeOn(true);
    var theDb = await openDatabase(
        join(await getDatabasesPath(), 'testcase.db'),
        version: 1,
        onCreate: _onCreate);
    return theDb;
  }

//  void _onUpgrade(Database db, int versio) async {
//    await db.execute("DROP TABLE ${GSD.tableSpecialDay}");
//    await db.execute(
//        "CREATE TABLE ${GSD.tableSpecialDay}(${GSD.sdId} INTEGER PRIMARY KEY, ${GSD.sdTanggal} TEXT,${GSD.sdArrayTanggal} TEXT, ${GSD.sdStringTanggal} TEXT)");
//  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE ${TbUser.tableName}(${TbUser.id} INTEGER PRIMARY KEY, "
        "${TbUser.username} TEXT, "
        "${TbUser.email} INTEGER,"
        "${TbUser.password} INTEGER)");
  }
}
