import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/repository/i_session_login_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

const is_logged_in = "is_logged_in";
const user_data = "user_data";

class SessionLoginRepository implements ISessionLoginRepo {
  final SharedPreferences sharedPreferences;
  SessionLoginRepository(this.sharedPreferences);
  @override
  Future<bool> isLogin() async {
    bool? isLoggedIn = await sharedPreferences.getBool(is_logged_in);
    if (isLoggedIn != null) {
      return isLoggedIn;
    }
    return false;
  }

  @override
  Future<void> setSessionLogin(bool isLoggedIn) async {
    await sharedPreferences.setBool(is_logged_in, isLoggedIn);
  }

  @override
  Future<User?> getCacheUser() async {
    String? userString = await sharedPreferences.getString(user_data);
    if (userString != null) {
      return User.fromJson(userString);
    }
    return null;
  }

  @override
  Future<void> setCacheUser(User user) async {
    await sharedPreferences.setString(user_data, user.toJson().toString());
  }
}
