import 'package:majootestcase/models/repository/i_user_repository.dart';
import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/services/common/database/databasehelper.dart';
import 'package:majootestcase/services/common/database/stringdbconst.dart';
import 'package:majootestcase/services/datamodel/userdata.dart';

class UserRepository implements IUserRepository {
  @override
  Future<User?> getUser(String email, String password) async {
    User? user;
    try {
      var dbClient = await (DatabaseHelper().db);
      String query =
          'SELECT * FROM ${TbUser.tableName} WHERE ${TbUser.email}=\'$email\' AND ${TbUser.password}=\'$password\'';
      print(query);
      List<Map> list = await dbClient!.rawQuery(query);

      for (int i = 0; i < list.length; i++) {
        user = UserData.fromMapDb(list[0] as Map<String, dynamic>);
      }

      return user;
    } catch (e) {
      return user;
    }
  }

  @override
  Future<int> save(User user) async {
    try {
      UserData userData = UserData.fromUser(user);
      var dbClient = await DatabaseHelper().db;

      int res = await dbClient!.insert(TbUser.tableName, userData.toMapDb());
      return res;
    } catch (e) {
      return 0;
    }
  }

  @override
  Future<User?> getUserById(int id) async {
    User? user;
    try {
      var dbClient = await (DatabaseHelper().db);
      List<Map> list = await dbClient!
          .rawQuery('SELECT * FROM ${TbUser.tableName} WHERE ${TbUser.id}=$id');

      for (int i = 0; i < list.length; i++) {
        user = UserData.fromMapDb(list[0] as Map<String, dynamic>);
      }

      return user;
    } catch (e) {
      return user;
    }
  }

  @override
  Future<bool> isTableUserEmpty() async {
    try {
      var dbClient = await (DatabaseHelper().db);
      List<Map> list =
          await dbClient!.rawQuery('SELECT * FROM ${TbUser.tableName} LIMIT 1');

      return list.isEmpty;
    } catch (e) {
      return true;
    }
  }
}
