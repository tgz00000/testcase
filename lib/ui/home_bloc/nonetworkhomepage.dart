import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/extra/error_screen.dart';

class NoNetworkHomepage extends StatelessWidget {
  const NoNetworkHomepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ErrorScreen(
      retry: () {},
      textColor: Colors.black,
      message: "Gangguan jaringan",
      errorSymbol: Icon(
        Icons.signal_wifi_off,
        size: 100,
        color: Colors.grey,
      ),
      retryButton: IconButton(
        onPressed: () {
          print("gak jalan");
          BlocProvider.of<HomeBlocCubit>(context).fetching_data();
        },
        iconSize: 30,
        icon: Icon(
          Icons.refresh,
          color: Colors.red,
        ),
      ),
      fontSize: 24,
    );
  }
}
