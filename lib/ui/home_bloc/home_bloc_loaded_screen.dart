import 'package:flutter/material.dart';
import 'package:majootestcase/models/entity/movie_detail.dart';
import 'package:majootestcase/ui/detailmovie/detailmoviepage.dart';
import 'package:path/path.dart';

class HomeBlocLoadedScreen extends StatefulWidget {
  final List<MovieDetail> data;

  const HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  State<HomeBlocLoadedScreen> createState() => _HomeBlocLoadedScreenState();
}

class _HomeBlocLoadedScreenState extends State<HomeBlocLoadedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: widget.data.length,
        itemBuilder: (context, index) {
          return movieItemWidget(context, widget.data[index]);
        },
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, MovieDetail data) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (ctx) => DetailMoviePage(movieDetail: data)));
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network(data.poster),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.title, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
