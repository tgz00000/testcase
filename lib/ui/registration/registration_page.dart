import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/registration_bloc/registration_bloc_cubit.dart';
import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/ui/component/button/mojoobutton.dart';
import 'package:majootestcase/ui/component/dialog/mojodialog.dart';
import 'package:majootestcase/ui/component/label/mojoolabel.dart';
import 'package:majootestcase/ui/component/textfield/textfield.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  late RegistrationBlocCubit _blocCubit;
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    _blocCubit = RegistrationBlocCubit();
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: LabelBlack.size1('Form Registrasi'),
        ),
        body: BlocListener<RegistrationBlocCubit, RegistrationBlocState>(
          bloc: _blocCubit,
          listener: (context, state) {
            if (state is RegistrationBlocSuccesState) {
              _showSnacbar('Register Berhasil');
              Navigator.of(context).pop();
              context.read<AuthBlocCubit>().login_success();
            }

            if (state is RegistrationBlocErrorState) {
              _showSnacbar("Gagal di simpan.");
              Navigator.of(context).pop();
            }

            if (state is RegistrationBlocLoadingState) {
              MojooDialog.loadingDialog(context);
            }
          },
          child: SizedBox(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    _cellForm("Username", _usernameController),
                    _cellEmail("Email", _emailController),
                    _cellPassword("Password", _passwordController),
                    ButtonModeSubmit(
                      text: "Registrasi",
                      borderColor: Colors.blue,
                      onTap: () {
                        _handleTapSubmit();
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget _cellForm(String label, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFieldUmum(
        text: label,
        controller: controller,
        validator: (value) {},
      ),
    );
  }

  Widget _cellEmail(String label, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFieldUmum(
        text: label,
        controller: controller,
        validator: (value) {
          final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
          if (value != null)
            return pattern.hasMatch(value)
                ? null
                : 'Masukkan e-mail yang valid';
        },
      ),
    );
  }

  Widget _cellPassword(String label, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:
          TextFieldPassword(text: "Password", controller: _passwordController),
    );
  }

  void _handleTapSubmit() {
    String username = _usernameController.text;
    String email = _emailController.text;
    String password = _emailController.text;
    if (username.isEmpty || email.isEmpty || password.isEmpty) {
      MojooDialog.generalDialogConfirm(context,
          'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
    } else {
      if (_formKey.currentState?.validate() == true) {
        User user = User(userName: username, password: password, email: email);
        _blocCubit.registration(user);
      } else {
        MojooDialog.generalDialogConfirm(context, 'Masukkan e-mail yang valid');
      }
    }
  }

  void _showSnacbar(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(message),
          ),
          backgroundColor: Colors.grey,
        ),
      );
  }
}
