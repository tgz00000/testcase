import 'package:flutter/material.dart';
import 'package:majootestcase/models/entity/movie.dart';
import 'package:majootestcase/models/entity/movie_detail.dart';

class DetailMoviePage extends StatelessWidget {
  final MovieDetail? movieDetail;
  const DetailMoviePage({Key? key, required this.movieDetail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (movieDetail == null) {
      return Scaffold(
        body: Center(
          child: Text('No Data'),
        ),
      );
    }
    String title = movieDetail!.title;
    String tahun = movieDetail!.tahun;
    String poster = movieDetail!.poster;
    List<Movie>? lmovie = movieDetail!.listSeries;
    if (lmovie == null) {
      lmovie = [];
    }
    Size size = MediaQuery.of(context).size;
    String text = '$title ($tahun)';
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Movie'),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(25),
                  child: Image.network(
                    poster,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  child: Text(text, textDirection: TextDirection.ltr),
                ),
                lmovie.isNotEmpty
                    ? SizedBox(
                        width: size.width - 40,
                        height: 200,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: lmovie.length,
                          itemBuilder: (context, index) {
                            return _cellImage(lmovie![index].poster);
                          },
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _cellImage(String pathImage) {
    return Padding(
      padding: EdgeInsets.all(4),
      child: Image.network(
        pathImage,
        scale: 1,
      ),
    );
  }
}
