import 'package:flutter/material.dart';

class TextFieldPassword extends StatefulWidget {
  final String text;
  final TextEditingController controller;

  TextFieldPassword({required this.text, required this.controller});

  @override
  _TextFieldPasswordState createState() => _TextFieldPasswordState();
}

class _TextFieldPasswordState extends State<TextFieldPassword> {
  late bool _isHidePassword;
  final TextStyle _labelStyle = TextStyle(color: Colors.grey[600]);
  final TextStyle _textfieldStyle = TextStyle(fontSize: 14);

  @override
  void initState() {
    _isHidePassword = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _label(widget.text),
        _entryFieldPassword(widget.controller),
      ],
    );
  }

  Widget _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        text,
        style: _labelStyle,
      ),
    );
  }

  Widget _entryFieldPassword(TextEditingController controller) {
    return TextFormField(
      style: _textfieldStyle,
      controller: controller,
      obscureText: _isHidePassword,
      autofocus: false,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        // hintText: text,
        suffixIcon: GestureDetector(
          onTap: () {
            _togglePasswordVisibility();
          },
          child: Icon(
            _isHidePassword ? Icons.visibility_off : Icons.visibility,
            color: _isHidePassword ? Colors.grey : Colors.blue,
          ),
        ),
        isDense: true,
      ),
    );
  }

  void _togglePasswordVisibility() {
    setState(() {
      _isHidePassword = !_isHidePassword;
    });
  }
}

class TextFieldLogin extends StatefulWidget {
  final String text;
  final TextEditingController controller;
  final String? Function(String?) validator;

  TextFieldLogin(
      {required this.text, required this.controller, required this.validator});

  @override
  _TextFieldLoginState createState() => _TextFieldLoginState();
}

class _TextFieldLoginState extends State<TextFieldLogin> {
  // * Before
  final TextStyle _labelStyle = TextStyle(color: Colors.grey[600]);
  // * After
  // final TextStyle _labelStyle = TextStyle(color: Color(0xFF644193));
  final TextStyle _textfieldStyle = TextStyle(fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _label(widget.text),
        _entryField(widget.controller),
      ],
    );
  }

  Widget _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        text,
        style: _labelStyle,
      ),
    );
  }

  Widget _entryField(TextEditingController controller) {
    return TextFormField(
      style: _textfieldStyle,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (str) {},
      validator: widget.validator,
      maxLines: 1,
      controller: controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        // labelText: text,
        isDense: true,
      ),
    );
  }
}

class TextFieldUmum extends StatefulWidget {
  final String text;
  final TextEditingController controller;
  final String? Function(String?) validator;

  TextFieldUmum(
      {required this.text, required this.controller, required this.validator});

  @override
  _TextFieldUmumState createState() => _TextFieldUmumState();
}

class _TextFieldUmumState extends State<TextFieldUmum> {
  // * Before
  final TextStyle _labelStyle = TextStyle(color: Colors.grey[600]);
  // * After
  // final TextStyle _labelStyle = TextStyle(color: Color(0xFF644193));
  final TextStyle _textfieldStyle = TextStyle(fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _label(widget.text),
        _entryField(widget.controller),
      ],
    );
  }

  Widget _label(String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        text,
        style: _labelStyle,
      ),
    );
  }

  Widget _entryField(TextEditingController controller) {
    return TextFormField(
      style: _textfieldStyle,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (str) {},
      validator: widget.validator,
      maxLines: 1,
      controller: controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        // labelText: text,
        isDense: true,
      ),
    );
  }
}
