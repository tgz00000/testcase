import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:majootestcase/ui/component/button/mojoobutton.dart';
import 'package:majootestcase/ui/component/label/mojoolabel.dart';

class MojooDialog {
  static Future loadingDialog(BuildContext context) {
    return showDialog<String>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => SimpleDialog(
              title: LabelApp.size1(
                'Loading...',
                color: Colors.red,
              ),
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              children: <Widget>[
                LoadingBouncingLine.circle(backgroundColor: Colors.deepOrange),
              ],
            ));
  }

  static Future<String?> generalDialogConfirm(
      BuildContext context, String? str) {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) => SimpleDialog(
              title: LabelApp.size1(
                'Confirm',
                color: Colors.red,
              ),
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      right: 16.0, left: 16.0, bottom: 3.0),
                  child: LabelBlack.size2(str),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      right: 16.0, left: 16.0, bottom: 3.0),
                  child: ButtonApp(
                      text: 'Ok',
                      colorButton: Colors.blue,
                      onTap: () {
                        Navigator.of(context).pop();
                      }),
                ),
              ],
            ));
  }
}
