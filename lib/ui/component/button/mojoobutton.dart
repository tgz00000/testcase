import 'package:flutter/material.dart';

class ButtonApp extends StatefulWidget {
  final Function onTap;
  final String? text;
  final Color? bgColor;
  final Color? colorButton;
  final bool enable;

  ButtonApp(
      {required this.text,
      required this.onTap,
      this.enable = true,
      this.bgColor = Colors.transparent,
      this.colorButton});

  @override
  _ButtonAppState createState() => _ButtonAppState();
}

class _ButtonAppState extends State<ButtonApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle style = TextStyle(
        color: widget.enable ? widget.colorButton : Colors.grey, fontSize: 12);
    return OutlinedButton(
      onPressed: widget.enable
          ? () {
              widget.onTap();
            }
          : null,
      child: Text(
        widget.text!,
        style: style,
      ),
      style: OutlinedButton.styleFrom(
        backgroundColor: widget.bgColor,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        // backgroundColor: Colors.white,
        primary: widget.colorButton,
        side: BorderSide(color: widget.colorButton!, width: 0.5),
      ),
    );
  }
}

class ButtonModeSubmit extends StatelessWidget {
  final String text;
  final Function onTap;
  final Color? borderColor;
  ButtonModeSubmit({required this.text, required this.onTap, this.borderColor});

  @override
  Widget build(BuildContext context) {
    return _loginButton();
  }

  Widget _loginButton() {
    Color _borderColor = borderColor ?? Colors.white;
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, left: 10, right: 10),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 5),
          // alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            border: Border.all(
              color: _borderColor,
              width: 0.5,
            ),
            // boxShadow: <BoxShadow>[
            //   BoxShadow(
            //       color: Colors.blue,
            //       offset: Offset(2, 4),
            //       blurRadius: 5,
            //       spreadRadius: 2)
            // ],
          ),
          child: Center(
            child: Text(
              text,
              style: const TextStyle(
                  fontSize: 14,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}
