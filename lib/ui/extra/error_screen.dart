import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? errorSymbol;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.errorSymbol,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              style: TextStyle(
                  fontSize: fontSize, color: textColor ?? Colors.black),
            ),
            SizedBox(
              height: 20,
            ),
            retry != null
                ? Column(
                    children: [
                      errorSymbol ??
                          Icon(
                            Icons.sms_failed,
                            size: 100,
                            color: Colors.red,
                          ),
                      SizedBox(
                        height: 20,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: () {
                              if (retry != null) retry!();
                            },
                            icon: Icon(
                              Icons.refresh_sharp,
                              color: Colors.blue,
                            ),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
