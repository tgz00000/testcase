import 'package:flutter/material.dart';
import 'package:majootestcase/ui/component/label/mojoolabel.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white70,
      child: Column(
        children: [
          Spacer(),
          LabelApp.size1("Selamat Datang di"),
          SizedBox(
            height: 20,
          ),
          LabelBlack.size1('Mojoo App'),
          Spacer(),
        ],
      ),
    );
  }
}
