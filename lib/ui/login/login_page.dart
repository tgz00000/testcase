import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/ui/component/dialog/mojodialog.dart';
import 'package:majootestcase/ui/component/textfield/textfield.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  // final _emailController = TextController(initialValue: "");
  // final _passwordController = TextController(initialValue: "");
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  late AuthBlocCubit _blocCubit;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    _blocCubit = AuthBlocCubit();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      bloc: _blocCubit,
      listener: (context, state) {
        print(state);
        if (state is AuthBlocLoggedInState) {
          _showSnacbar('Login berhasil');
          context.read<AuthBlocCubit>().login_success();
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (_) => BlocProvider(
          //       create: (context) => HomeBlocCubit()..fetching_data(),
          //       child: HomeBlocScreen(),
          //     ),
          //   ),
          // );
        }
        if (state is AuthBlocErrorState) {
          _showSnacbar(state.error);
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                OutlinedButton(onPressed: handleLogin, child: Text('Login')),
                // CustomButton(
                //   text: 'Login',
                //   onPressed: handleLogin,
                //   height: 100,
                // ),
                SizedBox(
                  height: 50,
                ),
                _register(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showSnacbar(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(message),
          ),
          backgroundColor: Colors.grey,
        ),
      );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          // CustomTextFormField(
          //   context: context,
          //   controller: _emailController,
          //   isEmail: true,
          //   hint: 'Example@123.com',
          //   label: 'Email',
          //   validator: (val) {
          //     final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
          //     if (val != null)
          //       return pattern.hasMatch(val) ? null : 'email is invalid';
          //   },
          // ),
          // CustomTextFormField(
          //   context: context,
          //   label: 'Password',
          //   hint: 'password',
          //   controller: _passwordController,
          //   isObscureText: _isObscurePassword,
          //   suffixIcon: IconButton(
          //     icon: Icon(
          //       _isObscurePassword
          //           ? Icons.visibility_off_outlined
          //           : Icons.visibility_outlined,
          //     ),
          //     onPressed: () {
          //       setState(() {
          //         _isObscurePassword = !_isObscurePassword;
          //       });
          //     },
          //   ),
          // ),

          TextFieldLogin(
            text: "Email",
            controller: _emailController,
            validator: (value) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (value != null)
                return pattern.hasMatch(value)
                    ? null
                    : 'Masukkan e-mail yang valid';
            },
          ),
          SizedBox(
            height: 10,
          ),
          TextFieldPassword(text: "Password", controller: _passwordController),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          context.read<AuthBlocCubit>().goToRegistration();
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    print('Login');
    final _email = _emailController.text;
    final _password = _passwordController.text;
    if (_email.isEmpty || _password.isEmpty) {
      MojooDialog.generalDialogConfirm(context,
          'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
    } else {
      if (formKey.currentState?.validate() == true) {
        User user = User(
          email: _email,
          password: _password,
          userName: '',
        );

        _blocCubit.login_user(user);
      } else {
        MojooDialog.generalDialogConfirm(context, 'Masukkan e-mail yang valid');
      }
    }
  }
}
