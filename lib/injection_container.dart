import 'package:get_it/get_it.dart';
import 'package:majootestcase/models/repository/i_user_repository.dart';
import 'package:majootestcase/services/local_repository/userrepository.dart';

final getIt = GetIt.instance;

Future<void> init() async {
  getIt.registerLazySingleton<IUserRepository>(() => UserRepository());
}
