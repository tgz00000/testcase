import 'package:majootestcase/models/entity/movie.dart';

class MovieDetail {
  final String title;
  final String poster;
  String tahun;
  MovieDetail(
      {required this.title,
      required this.poster,
      this.listSeries,
      required this.tahun});

  List<Movie>? listSeries;
}
