import 'dart:convert';

class User {
  String email;
  String userName;
  String password;

  User({
    required this.email,
    required this.userName,
    required this.password,
  });

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'userName': userName,
      'password': password,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      email: map['email'] ?? '',
      userName: map['userName'] ?? '',
      password: map['password'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());
  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  User copyWith({
    String? email,
    String? userName,
    String? password,
  }) {
    return User(
      email: email ?? this.email,
      userName: userName ?? this.userName,
      password: password ?? this.password,
    );
  }

  @override
  String toString() =>
      'User(email: $email, userName: $userName, password: $password)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is User &&
        other.email == email &&
        other.userName == userName &&
        other.password == password;
  }

  @override
  int get hashCode => email.hashCode ^ userName.hashCode ^ password.hashCode;
}
