import '../entity/user.dart';

abstract class IUserRepository {
  Future<User?> getUser(String email, String password);
  Future<int> save(User user);
  Future<User?> getUserById(int id);
  Future<bool> isTableUserEmpty();
}
