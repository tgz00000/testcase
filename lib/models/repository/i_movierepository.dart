import 'package:majootestcase/models/entity/movie_detail.dart';

abstract class IMovieRepository {
  Future<List<MovieDetail>?> getMovieList();
}
