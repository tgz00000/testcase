import 'package:majootestcase/models/entity/user.dart';

abstract class ISessionLoginRepo {
  Future<bool> isLogin();
  Future<void> setSessionLogin(bool isLoggedIn);
  Future<User?> getCacheUser();
  Future<void> setCacheUser(User user);
}
