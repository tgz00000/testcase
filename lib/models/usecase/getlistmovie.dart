import 'package:majootestcase/models/entity/movie_detail.dart';
import 'package:majootestcase/models/repository/i_movierepository.dart';

class GetMovie {
  IMovieRepository iMovieRepository;
  GetMovie({required this.iMovieRepository});
  Future<List<MovieDetail>?> getListMovieDetail() async {
    return await iMovieRepository.getMovieList();
  }
}
