import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/repository/i_user_repository.dart';

class Registration {
  IUserRepository userRepository;
  Registration({required this.userRepository});
  Future<User?> submitRegistrationAndGetDataBack(User user) async {
    int res = await userRepository.save(user);
    print('res $res');
    if (res > 0) {
      return await userRepository.getUserById(res);
    }
    return null;
  }

  Future<bool> isTableUserEmpty() async {
    return await userRepository.isTableUserEmpty();
  }
}
