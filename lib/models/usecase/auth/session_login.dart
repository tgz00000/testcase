import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/repository/i_session_login_repository.dart';

class SessionLogin {
  ISessionLoginRepo sessionLoginRepo;
  SessionLogin({required this.sessionLoginRepo});

  Future<bool> isLogin() async {
    return await sessionLoginRepo.isLogin();
  }

  Future<void> setSessionLogin(bool isLoggedIn) async {
    await sessionLoginRepo.setSessionLogin(isLoggedIn);
  }

  Future<User?> getCacheUser() async {
    return await sessionLoginRepo.getCacheUser();
  }

  Future<void> setCacheUser(User user) async {
    await sessionLoginRepo.setCacheUser(user);
  }
}
