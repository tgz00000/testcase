import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/repository/i_user_repository.dart';

class SubmitLogin {
  IUserRepository userRepository;
  SubmitLogin({required this.userRepository});
  Future<User?> submitLogin(String email, String password) async {
    return await userRepository.getUser(email, password);
  }
}
