import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/usecase/auth/registration.dart';
import 'package:majootestcase/models/usecase/auth/session_login.dart';
import 'package:majootestcase/models/usecase/auth/submitlogin.dart';
import 'package:majootestcase/services/local_repository/session_login_repository.dart';
import 'package:majootestcase/services/local_repository/userrepository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async {
    emit(AuthBlocInitialState());

    _getLoginSession().then((isLoggedIn) {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    });
  }

  Future<bool> _getLoginSession() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    SessionLogin sessionLogin = SessionLogin(
        sessionLoginRepo: SessionLoginRepository(sharedPreferences));
    return await sessionLogin.isLogin();
  }

  void login_user(User user) {
    emit(AuthBlocLoadingState());
    _loginUser(user).then((issuccess) {
      if (issuccess) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocErrorState("Login gagal , periksa kembali inputan anda"));
      }
    });
  }

  Future<bool> _loginUser(User user) async {
    SubmitLogin submitLogin = SubmitLogin(userRepository: UserRepository());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    SessionLogin sessionLogin = SessionLogin(
        sessionLoginRepo: SessionLoginRepository(sharedPreferences));
    User? tmpUser = await submitLogin.submitLogin(user.email, user.password);
    if (tmpUser == null) {
      return false;
    } else {
      sessionLogin.setSessionLogin(true);
      sessionLogin.setCacheUser(user);
      return true;
    }
  }

  void goToRegistration() {
    emit(AuthBlocRegistration());
  }

  void login_success() {
    emit(AuthBlocLoggedInState());
  }
}
