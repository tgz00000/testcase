import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/entity/user.dart';
import 'package:majootestcase/models/usecase/auth/registration.dart';
import 'package:majootestcase/models/usecase/auth/session_login.dart';
import 'package:majootestcase/services/local_repository/session_login_repository.dart';
import 'package:majootestcase/services/local_repository/userrepository.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'registration_bloc_state.dart';

class RegistrationBlocCubit extends Cubit<RegistrationBlocState> {
  RegistrationBlocCubit() : super(RegistrationBlocInitialState());

  void fetch_history_login() async {
    emit(RegistrationBlocInitialState());
  }

  void registration(User user) async {
    emit(RegistrationBlocLoadingState());
    _registration(user).then((usr) {
      if (usr == null) {
        emit(RegistrationBlocErrorState("Registrasi gagal"));
      } else {
        emit(RegistrationBlocSuccesState());
      }
    });
  }

  Future<User?> _registration(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Registration registration = Registration(userRepository: UserRepository());
    SessionLogin sessionLogin = SessionLogin(
        sessionLoginRepo: SessionLoginRepository(sharedPreferences));
    User? tmpUser = await registration.submitRegistrationAndGetDataBack(user);
    sessionLogin.setSessionLogin(true);
    sessionLogin.setCacheUser(user);
    return tmpUser;
  }
}
