part of 'registration_bloc_cubit.dart';

abstract class RegistrationBlocState extends Equatable {
  const RegistrationBlocState();

  @override
  List<Object> get props => [];
}

class RegistrationBlocInitialState extends RegistrationBlocState {}

class RegistrationBlocLoadingState extends RegistrationBlocState {}

class RegistrationBlocSuccesState extends RegistrationBlocState {}

class RegistrationBlocErrorState extends RegistrationBlocState {
  final error;

  RegistrationBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
