import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/entity/movie_detail.dart';
import 'package:majootestcase/models/usecase/getlistmovie.dart';
import 'package:majootestcase/services/remote_repository/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetching_data() async {
    emit(HomeBlocInitialState());
    GetMovie getMovie = GetMovie(iMovieRepository: ApiServices());

    List<MovieDetail>? lmovieDetail = await getMovie.getListMovieDetail();
    if (lmovieDetail == null) {
      emit(HomeBlocErrorState("Error Unknown"));
    } else {
      emit(HomeBlocLoadedState(lmovieDetail));
    }
  }
}
